CONTENTS OF THIS FILE
---------------------

* Module Summary
* Optional Requirements
* Installation
* Administration

MODULE SUMMARY
--------------

Create a nice looking PDF communique with letterhead for either e-mail or print
correspondence.

Adds a PDF tab to the node edit form. The WYSIWYG output will not match the PDF
perfectly due to the fact that PDF exists in "print" while the WYSIWYG is 
browser based and thus treats text differently. Things that have sizes defined
explicitly such as HTML tables or images should be pretty dead-on.

OPTIONAL REQUIREMENTS
---------------------

While just following the installation instructions below will give you a module
that puts your plain text onto a PDF with the click of a button, it really
shines when you have a WYSIWYG creating some nice HTML.

Enable the Correspondence Content Type module (feature) and get a preconfigured
WYSIWYG editor attached to your HTML text format.  Other than downloading the
dependencies (listed below), this makes getting a decent baseline editor and 
PDF experience really fast and easy.

* Features
* Pathauto (and Token dependency)
* Strongarm
* Chaos tools
* Wysiwyg
* IMCE Wysiwyg API bridge

INSTALLATION
------------

It is highly recommended you just download the modules listed in OPTIONAL
REQUIREMENTS and enable the Correspondence Content Type module for a more
complete one-click install.

Otherwise, continue...

This module won't function at all without the Libraries API module installed
and TCPDF placed into the 'TCPDF' directory in the libraries folder.  One
common default location for libraries is 'sites/all/libraries', so TCPDF
should be in 'sites/all/libraries/tcpdf'.

This module also requires IMCE in order to make picking assets easier.

This module also depends on the private files directory being configured
correctly!  Go to 'admin/config/media/file-system' and set the directory
accordingly.

ADMINISTRATION
--------------

Head to 'admin/config/content/correspondence' to set just about everything
there is that can be set.  Stick to just the basic settings at first.
