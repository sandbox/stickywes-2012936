<?php
/**
 * @file
 * correspondence_content_type.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function correspondence_content_type_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_correspondence_pattern';
  $strongarm->value = 'correspondence/[node:title]';
  $export['pathauto_node_correspondence_pattern'] = $strongarm;

  return $export;
}
