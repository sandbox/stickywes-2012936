<?php
/**
 * @file
 * correspondence_content_type.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function correspondence_content_type_filter_default_formats() {
  $formats = array();

  // Exported format: textHTML.
  $formats['texthtml'] = array(
    'format' => 'texthtml',
    'name' => 'textHTML',
    'cache' => 1,
    'status' => 1,
    'weight' => 0,
    'filters' => array(),
  );

  return $formats;
}
