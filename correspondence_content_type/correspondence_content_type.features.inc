<?php
/**
 * @file
 * correspondence_content_type.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function correspondence_content_type_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function correspondence_content_type_node_info() {
  $items = array(
    'correspondence' => array(
      'name' => t('correspondence'),
      'base' => 'node_content',
      'description' => t('A simple content type that is used to generate pretty looking PDFs that can be used as letters in direct mail.'),
      'has_title' => '1',
      'title_label' => t('Subject/Title'),
      'help' => t('Keep things simple, you\'re just writing a letter and not making a web page.'),
    ),
  );
  return $items;
}
