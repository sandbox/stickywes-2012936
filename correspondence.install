<?php
/**
 * @file
 * The Correspondence install file.
 *
 * Prepares the file directory that PDFs will be saved to.
 */

if (!defined('CORRESPONDENCE_BASE_DIR')) {
  define('CORRESPONDENCE_BASE_DIR', 'private://correspondence');
}

/**
 * Implements hook_enable().
 */
function correspondence_enable() {
  $base = CORRESPONDENCE_BASE_DIR;
  $wrappers = stream_get_wrappers();
  $directory_ready = FALSE;
  if (in_array('private', $wrappers)) {
    $directory_ready = file_prepare_directory($base, FILE_CREATE_DIRECTORY);
  }
  if (!$directory_ready) {
    drupal_set_message(t("Correspondence module could not create the private directory it needed.  Please verify your file settings."), 'warning');
  }

  /*
   * Insert our provided template files into the Drupal file system for easier access in IMCE windows.
   */
  $files = file_scan_directory(drupal_get_path('module', 'correspondence'). '/assets', '/\.(eps|ai)$/');
  foreach ($files as $filepath => $file) {
    $scheme = file_default_scheme();
    $basename = drupal_basename($filepath);
    $data = file_get_contents($filepath);
    $filepath = $scheme . '://' . $basename;

    $file = file_save_data($data, $filepath, FILE_EXISTS_REPLACE);
  }
}

/**
 * Implements hook_requirements().
 */
function correspondence_requirements($phase) {
  $t = get_t();
  $requirements = array();
  $base = CORRESPONDENCE_BASE_DIR;
  $wrappers = stream_get_wrappers();
  $directory_ready = FALSE;
  if (in_array('private', $wrappers)) {
    $directory_ready = file_prepare_directory($base, FILE_CREATE_DIRECTORY);
  }
  if (!$directory_ready) {
    if ($phase == 'runtime') {
      $severity = REQUIREMENT_ERROR;
    }
    else {
      $severity = REQUIREMENT_WARNING;
    }

    $requirements['correspondence_private'] = array(
      'title' => $t('Correspondence PDF Storage'),
      'value' => $t('Unable to create directory'),
      'description' => $t('The private file directory is required for the Correspondence Module but it does not appear to be configured correctly (could not create or access %dir)', array('%dir' => $base)),
      'severity' => $severity,
    );
  }
  if ($phase == 'runtime') {
    $library = libraries_detect('tcpdf');
    if (!$library['installed']) {
      $t_args = array('!error' => filter_xss($library['error message']));
      $requirements['correspondence_tcpdf'] = array(
        'title' => $t('Correspondence Library Requirement'),
        'value' => $t('TCPDF Library could not be loaded'),
        'description' => $t("The TCPDF Library that the Correspondence module depends on could not be loaded (!error).  Please verify library's location.", $t_args),
        'severity' => REQUIREMENT_ERROR,
      );
    }
  }

  return $requirements;
}

/**
 * Implements hook_uninstall().
 */
function correspondence_uninstall() {
  // Simple DB query to get the names of our variables.  Thanks, example module!
  $results = db_select('variable', 'v')
    ->fields('v', array('name'))
    ->condition('name', 'correspondence_%', 'LIKE')
    ->execute();
  // Loop through and delete each of our variables.
  foreach ($results as $result) {
    variable_del($result->name);
  }
}
