(function ($) {

  Drupal.behaviors.correspondenceIMCELoad = {
    target: {},
    attach: function (context, settings) {
      $('.imce-target', context).click(function(e) {
        Drupal.behaviors.correspondenceIMCELoad.target = e.target;
        var path = settings.basePath;
        if (path != "undefined") {
          path = settings.basePath + 'imce';
          window.open(path + '?app=drupalCorrespondence|sendto@Drupal.behaviors.correspondenceIMCELoad.fileHandler|bubbagump@peeps', '', 'width=560,height=360,resizable=1');
        }
        return false;
      });
    },
    fileHandler: function(file, win) {
      $(Drupal.behaviors.correspondenceIMCELoad.target).val(file.url);
      win.close();
      return;
    }
  };

})(jQuery);
