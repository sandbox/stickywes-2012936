<?php
/**
 * @file
 * The administration pages for the Correspondence module.
 */

/**
 * Page callback: Creates the settings form for the module.
 *
 * @see system_settings_form()
 */
function correspondence_settings_page() {
  $form = array();
  module_load_include('install', 'correspondence');
  $reqs = array();
  $reqs = correspondence_requirements('runtime');
  foreach ($reqs as $requirement) {
    drupal_set_message($requirement['description'], 'error', TRUE);
  }

  $form['#attached']['js'] = array(
    drupal_get_path('module', 'correspondence') . '/js/correspondence.js',
  );

  if (module_exists('imce')) {
    $imce_description = t('Use an Adobe Illustrator file (extension *.ai or *.eps) as a header image.  Click the box to find/upload your file.');
    $imce_classes = array('imce-target');
  }
  else {
    $imce_description = '';
    $imce_classes = array();
  }

  $filename = CORRESPONDENCE_BASE_DIR . '/config_sample.pdf';
  $url = file_create_url($filename);
  if (!file_exists($filename)) {
    $preview_markup = '<p>PDF preview not found.  Try submitting this page!</p>';
  }
  else {
    $preview_markup = '<object data="' . $url . '" type="application/pdf" width="350px" height="500px">  <p>It appears you don\'t have a PDF plugin for this browser.  No biggie... you can <a href="' . $url . '">click here to download the PDF file.</a></p></object>';
  }
  $form['pdf_preview'] = array(
    '#type' => 'markup',
    '#markup' => $preview_markup,
  );
  $form['correspondence_pdf_author'] = array(
    '#type' => 'textfield',
    '#title' => t('PDF Author'),
    '#default_value' => variable_get('correspondence_pdf_author', CORRESPONDENCE_PDF_AUTHOR_DEFAULT),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['correspondence_pdf_creator'] = array(
    '#type' => 'textfield',
    '#title' => t('PDF Creator'),
    '#default_value' => variable_get('correspondence_pdf_creator', CORRESPONDENCE_PDF_CREATOR_DEFAULT),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['correspondence_header_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to the header file'),
    '#description' => t('Click the box to open a window to upload/choose your file.'),
    '#default_value' => variable_get('correspondence_header_path', CORRESPONDENCE_HEADER_PATH_DEFAULT),
    '#size' => 60,
    '#maxlength' => 128,
    '#description' => $imce_description,
    '#required' => TRUE,
    '#attributes' => array(
      'class' => $imce_classes,
    ),
    '#value_callback' => 'correspondence_admin_file_path_value',
  );

  $form['correspondence_footer_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to the footer file'),
    '#default_value' => variable_get('correspondence_footer_path', CORRESPONDENCE_FOOTER_PATH_DEFAULT),
    '#size' => 60,
    '#description' => t('Click the box to open a window to upload/choose your file.'),
    '#maxlength' => 128,
    '#required' => TRUE,
    '#description' => $imce_description,
    '#attributes' => array(
      'class' => $imce_classes,
    ),
    '#value_callback' => 'correspondence_admin_file_path_value',
  );

  /*** Advanced options ***/
  // @TODO: Hide these options under a collapsible div/fieldset.
  $form['correspondence_page_delimiter'] = array(
    '#type' => 'textfield',
    '#title' => t('Page Delimiter'),
    '#default_value' => variable_get('correspondence_page_delimiter', CORRESPONDENCE_PAGE_DELIMITER_DEFAULT),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['correspondence_page_orientation'] = array(
    '#type' => 'textfield',
    '#title' => t('Page Orientation'),
    '#default_value' => variable_get('correspondence_page_orientation', CORRESPONDENCE_PAGE_ORIENTATION_DEFAULT),
    '#size' => 60,
    '#description' => '(P=portrait, L=landscape)',
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['correspondence_pdf_unit'] = array(
    '#type' => 'textfield',
    '#title' => t('PDF Unit'),
    '#default_value' => variable_get('correspondence_pdf_unit', CORRESPONDENCE_PDF_UNIT_DEFAULT),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  // @todo: Make this a select list.
  $form['correspondence_page_format'] = array(
    '#type' => 'textfield',
    '#title' => t('PDF Page Format'),
    '#default_value' => variable_get('correspondence_page_format', CORRESPONDENCE_PAGE_FORMAT_DEFAULT),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['correspondence_pdf_encoding'] = array(
    '#type' => 'textfield',
    '#title' => t('PDF Encoding'),
    '#default_value' => variable_get('correspondence_pdf_encoding', CORRESPONDENCE_PDF_ENCODING_DEFAULT),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  // @todo: Make this a select list, with options derived from available fonts.
  $form['correspondence_pdf_font_family_main'] = array(
    '#type' => 'textfield',
    '#title' => t('Font Family (main)'),
    '#default_value' => variable_get('correspondence_pdf_font_family_main', 'helvetica'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['correspondence_pdf_font_style_main'] = array(
    '#type' => 'textfield',
    '#title' => t('Font Style (main)'),
    '#default_value' => variable_get('correspondence_pdf_font_style_main', CORRESPONDENCE_PDF_FONT_STYLE_MAIN_DEFAULT),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => FALSE,
  );

  $form['correspondence_pdf_font_size_main'] = array(
    '#type' => 'textfield',
    '#title' => t('PDF Font Size (main)'),
    '#default_value' => variable_get('correspondence_pdf_font_size_main', CORRESPONDENCE_PDF_FONT_SIZE_MAIN_DEFAULT),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['correspondence_pdf_font_family_mono'] = array(
    '#type' => 'textfield',
    '#title' => t('PDF Font Family (monospaced)'),
    '#default_value' => variable_get('correspondence_pdf_font_family_mono', CORRESPONDENCE_PDF_FONT_FAMILY_MONO_DEFAULT),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['correspondence_pdf_margin_left'] = array(
    '#type' => 'textfield',
    '#title' => t('Left Margin'),
    '#default_value' => variable_get('correspondence_pdf_margin_left', CORRESPONDENCE_PDF_MARGIN_LEFT_DEFAULT),
    '#size' => 60,
    '#maxlength' => 10,
    '#required' => TRUE,
  );

  $form['correspondence_pdf_margin_right'] = array(
    '#type' => 'textfield',
    '#title' => t('Right Margin'),
    '#default_value' => variable_get('correspondence_pdf_margin_right', CORRESPONDENCE_PDF_MARGIN_RIGHT_DEFAULT),
    '#size' => 60,
    '#maxlength' => 10,
    '#required' => TRUE,
  );

  $form['correspondence_pdf_margin_bottom'] = array(
    '#type' => 'textfield',
    '#title' => t('Bottom Content Edge'),
    '#default_value' => variable_get('correspondence_pdf_margin_bottom', CORRESPONDENCE_PDF_MARGIN_BOTTOM_DEFAULT),
    '#size' => 60,
    '#maxlength' => 10,
    '#required' => TRUE,
  );

  $form['correspondence_pdf_margin_top'] = array(
    '#type' => 'textfield',
    '#title' => t('Top Content Edge'),
    '#default_value' => variable_get('correspondence_pdf_margin_top', CORRESPONDENCE_PDF_MARGIN_TOP_DEFAULT),
    '#size' => 60,
    '#maxlength' => 10,
    '#required' => TRUE,
  );

  $form['correspondence_pdf_scale_ratio'] = array(
    '#type' => 'textfield',
    '#title' => t('Pixel Scale Ratio'),
    '#default_value' => variable_get('correspondence_pdf_scale_ratio', CORRESPONDENCE_PDF_SCALE_RATIO_DEFAULT),
    '#size' => 60,
    '#maxlength' => 10,
    '#required' => TRUE,
  );

  // The remainder of this form is mostly pulled straight from
  // system_settings_form().
  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }

  $form['#submit'][] = 'correspondence_config_submit';
  $form['#theme'] = 'system_settings_form';
  return $form;
}

/**
 * Submission handler for correspondence_settings_page().
 *
 * If values are changed, the PDF will be re-created.
 *
 * @see system_settings_form_submit()
 */
function correspondence_config_submit($form, &$form_state) {
  // Exclude unnecessary elements.
  form_state_values_clean($form_state);

  $filename = CORRESPONDENCE_BASE_DIR . '/config_sample.pdf';

  $build_pdf = !file_exists($filename);

  foreach ($form_state['values'] as $key => $value) {
    if (is_array($value) && isset($form_state['values']['array_filter'])) {
      $value = array_keys(array_filter($value));
    }

    if (!$build_pdf) {
      $build_pdf = (variable_get($key) != $value);
    }
    variable_set($key, $value);
  }

  // The file didn't exist or something changed, so we need a new PDF.
  if ($build_pdf) {
    correspondence_admin_preview_render();
  }

  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * A value callback for the file locations in the configuration form.
 *
 * IMCE returns URLs and not actual file paths, so the value to be stored needs
 * to be processed and converted into a path to be used by the module.
 */
function correspondence_admin_file_path_value($element, $input = FALSE, $form_state = array()) {
  $val = '';
  if ($input !== FALSE && $input !== NULL) {
    $val = str_replace(array("\r", "\n"), '', $input);
  }
  if (isset($element['#default_value'])) {
    $val = $element['#default_value'];
  }

  if (substr($val, 0, 1) == '/') {
    $val = substr($val, strlen($GLOBALS['base_path']));
  }

  return $val;
}

/**
 * Creates the preview PDF and saves it to the Drupal filesystem.
 */
function correspondence_admin_preview_render() {
  $preview_text = <<<PREVIEW_HTML
<p>1234 1st Stret Name<br />
Suite #1<br />
Town Or City, State, ZIPCODE<br />
Country Name
</p>

<p>Recipient Company or Organatization
ATTN: Recipient Name or Department
5678 Company Street Name
Office #1
City of Recipient, State, ZIPCODE
Country Name
</p>
<p>To Whom It May Concern,</p>
<p>Our product is extremely important to the continued representation of your organization within its industry.  Without our product, your brand recognition efforts will be inadequate as your competitors take advantage of our fine product.  Our high quality widget selections will help you do some amazing things in your industry, if only you make the effort to take advantage of this limited time offer.</p>
<p><table style="width: 254px; height: 76px;" border="1"><tbody><tr><td>Companies</td><td>Before Product</td><td>After Product</td></tr><tr><td>Company A</td><td>-100</td><td>300</td></tr><tr><td>Company B</td><td>20</td><td>492</td></tr></tbody></table><br /></p>
<p>As illustrated above, our widgets can turn even negatives into positives!  It is time for your organization to embrace our technological breakthroughs and add yourselves to that table of great expectations.</p>
<p>Sincerely Yours,<br />
Representative Person<br />
From Company<br />
With Phone Number<br /></p>
PREVIEW_HTML;

  $data = correspondence_get_config();

  $data['body_text'] = $preview_text;
  $data['outline_sections'] = TRUE;
  $pdf = correspondence_assemble_pdf($data);

  $dir = CORRESPONDENCE_BASE_DIR;
  $filename = $dir . '/config_sample.pdf';

  correspondence_prepare_directory($dir, FILE_CREATE_DIRECTORY);
  $file = correspondence_file_save_data($pdf, $filename);

  return $file;
}
